package de.prwh.cobaltmod.core.blocks.slabs;

public class BlockCobaltDoubleSlab extends CobaltBrickSlab {

	@Override
	public boolean isDouble() {
		return true;
	}

}
