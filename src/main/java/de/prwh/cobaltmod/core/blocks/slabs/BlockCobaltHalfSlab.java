package de.prwh.cobaltmod.core.blocks.slabs;

public class BlockCobaltHalfSlab extends CobaltBrickSlab {

	public BlockCobaltHalfSlab() {
	}
	
	@Override
	public boolean isDouble() {
		return false;
	}

}
