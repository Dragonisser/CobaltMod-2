package de.prwh.cobaltmod.core.items;

import net.minecraft.item.ItemSpade;

public class ItemCobaltShovel extends ItemSpade {

	public ItemCobaltShovel(ToolMaterial material) {
		super(material);
		this.setUnlocalizedName("cobalt_shovel");
		this.setRegistryName("cobalt_shovel");
	}
}
